def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board, index):
    print_board(board)
    print("GAME OVER")
    print(board[index], "has won")
    exit()

def is_row_winner(board, row_number):
    if row_number == 1:
        if board[row_number-1] == board[row_number] and board[row_number] == board[row_number+1]:
            return True
    elif row_number == 2:
        if board[row_number+1] == board[row_number+2] and board[row_number+2] == board[row_number+3]:
            return True
    elif row_number == 3:
        if board[row_number+3] == board[row_number+4] and board[row_number+4] == board[row_number+5]:
            return True

def is_column_winner(board, column_number):
    if column_number == 1:
        if board[column_number-1] == board[column_number+2] and board[column_number+2] == board[column_number+5]:
            return True
    elif column_number == 2:
        if board[column_number-1] == board[column_number+2] and board[column_number+2] == board[column_number+5]:
            return True
    elif column_number == 3:
        if board[column_number-1] == board[column_number+2] and board[column_number+2] == board[column_number+5]:
            return True

def is_diagonal_winner(board, diagonal_number):
    if diagonal_number == 1:
        if board[diagonal_number-1] == board[diagonal_number+3] and board[diagonal_number+3] == board[diagonal_number+7]:
            return True
    elif diagonal_number == 2:
        if board[diagonal_number] == board[diagonal_number+2] and board[diagonal_number+2] == board[diagonal_number+4]:
            return True

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(board, 0)
    elif is_row_winner(board, 2):
        game_over(board, 3)
    elif is_row_winner(board, 3):
        game_over(board, 6)
    elif is_column_winner(board, 1):
        game_over(board, 0)
    elif is_column_winner(board, 2):
        game_over(board, 1)
    elif is_column_winner(board, 3):
        game_over(board, 2)
    elif is_diagonal_winner(board, 1):
        game_over(board, 0)
    elif is_diagonal_winner(board, 2):
        game_over(board, 0)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
